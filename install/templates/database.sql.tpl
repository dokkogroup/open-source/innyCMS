# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.37)
# Database: inny_cms
# Generation Time: 2022-04-17 14:31:18 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table innydb_bucket
# ------------------------------------------------------------

DROP TABLE IF EXISTS `innydb_bucket`;

CREATE TABLE `innydb_bucket` (
`id_bucket` int(10) unsigned NOT NULL AUTO_INCREMENT,
`size` int(10) unsigned DEFAULT NULL,
`name` varchar(200) DEFAULT NULL,
`mime` varchar(127) NOT NULL,
`type` varchar(45) DEFAULT NULL,
`tags` text,
`count` int(10) unsigned NOT NULL DEFAULT '0',
`usages` longtext,
`hash` char(128) NOT NULL,
`aud_ins_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
`aud_upd_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
`aud_ins_user` varchar(100) NOT NULL,
`aud_upd_user` varchar(100) NOT NULL,
PRIMARY KEY (`id_bucket`),
UNIQUE KEY `id_bucket_UNIQUE` (`id_bucket`),
FULLTEXT KEY `tags_INDEX` (`tags`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;



# Dump of table innydb_bucket_chunk
# ------------------------------------------------------------

DROP TABLE IF EXISTS `innydb_bucket_chunk`;

CREATE TABLE `innydb_bucket_chunk` (
`id_bucket_chunk` int(10) unsigned NOT NULL AUTO_INCREMENT,
`id_bucket` int(10) unsigned NOT NULL,
`next_chunk` int(10) unsigned DEFAULT NULL,
`data` mediumblob NOT NULL,
`aud_ins_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
`aud_upd_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
`aud_ins_user` varchar(100) NOT NULL,
`aud_upd_user` varchar(100) NOT NULL,
PRIMARY KEY (`id_bucket_chunk`),
KEY `id_bucket_index` (`id_bucket`) USING BTREE,
CONSTRAINT `id_bucket_fk` FOREIGN KEY (`id_bucket`) REFERENCES `innydb_bucket` (`id_bucket`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;



# Dump of table innydb_collection
# ------------------------------------------------------------

DROP TABLE IF EXISTS `innydb_collection`;

CREATE TABLE `innydb_collection` (
`id_collection` int(10) unsigned NOT NULL AUTO_INCREMENT,
`public_id` varchar(10) NOT NULL,
`name` varchar(100) NOT NULL,
`site_name` varchar(40) NOT NULL,
`metadata` longtext,
`aud_ins_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
`aud_upd_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
`aud_ins_user` varchar(100) NOT NULL,
`aud_upd_user` varchar(100) NOT NULL,
PRIMARY KEY (`id_collection`),
KEY `public_id_INDEX` (`public_id`),
KEY `name_INDEX` (`name`),
KEY `site_name_index` (`site_name`),
KEY `name_site_name_index` (`name`,`site_name`),
CONSTRAINT `FK_site_name_collection` FOREIGN KEY (`site_name`) REFERENCES `innydb_site` (`public_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;



# Dump of table innydb_document
# ------------------------------------------------------------

DROP TABLE IF EXISTS `innydb_document`;

CREATE TABLE `innydb_document` (
`id_document` int(10) unsigned NOT NULL AUTO_INCREMENT,
`public_id` varchar(10) NOT NULL,
`collection_name` varchar(100) NOT NULL,
`site_name` varchar(40) NOT NULL,
`position` int(10) unsigned NOT NULL DEFAULT '1',
`status` int(1) unsigned zerofill NOT NULL DEFAULT '1',
`field1` longtext,
`field2` longtext,
`field3` longtext,
`field4` longtext,
`field5` longtext,
`field6` longtext,
`field7` longtext,
`field8` longtext,
`field9` longtext,
`field10` longtext,
`field11` longtext,
`field12` longtext,
`field13` longtext,
`field14` longtext,
`field15` longtext,
`field16` longtext,
`field17` longtext,
`field18` longtext,
`field19` longtext,
`field20` longtext,
`field21` longtext,
`field22` longtext,
`field23` longtext,
`field24` longtext,
`field25` longtext,
`field26` longtext,
`field27` longtext,
`field28` longtext,
`field29` longtext,
`field30` longtext,
`files` mediumtext,
`aud_ins_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
`aud_upd_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
`aud_ins_user` varchar(100) NOT NULL,
`aud_upd_user` varchar(100) NOT NULL,
`draft` longtext,
PRIMARY KEY (`id_document`),
KEY `public_id_INDEX` (`public_id`),
KEY `public_id_collection_name_INDEX` (`public_id`,`collection_name`),
KEY `collection_name_INDEX` (`collection_name`),
KEY `site_name_INDEX` (`site_name`),
KEY `status_INDEX` (`status`),
CONSTRAINT `FK_collection_name` FOREIGN KEY (`collection_name`) REFERENCES `innydb_collection` (`name`) ON UPDATE CASCADE,
CONSTRAINT `FK_site_name` FOREIGN KEY (`site_name`) REFERENCES `innydb_site` (`public_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;



# Dump of table innydb_site
# ------------------------------------------------------------

DROP TABLE IF EXISTS `innydb_site`;

CREATE TABLE `innydb_site` (
`id_site` int(10) unsigned NOT NULL AUTO_INCREMENT,
`name` varchar(100) NOT NULL,
`public_id` varchar(40) NOT NULL,
`url` varchar(200) NOT NULL,
`metadata` longtext,
`status` char(1) NOT NULL DEFAULT '1',
`configs` longtext,
`aud_ins_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
`aud_upd_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
`aud_ins_user` varchar(100) NOT NULL,
`aud_upd_user` varchar(100) NOT NULL,
PRIMARY KEY (`id_site`),
UNIQUE KEY `public_id_UNIQUE` (`public_id`),
KEY `public_id_INDEX` (`public_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;



# Dump of table innydb_user
# ------------------------------------------------------------

DROP TABLE IF EXISTS `innydb_user`;

CREATE TABLE `innydb_user` (
`id_user` int(10) unsigned NOT NULL AUTO_INCREMENT,
`username` varchar(40) NOT NULL,
`password` varchar(100) NOT NULL,
`name` varchar(100) NOT NULL,
`lastname` varchar(100) NOT NULL,
`email` varchar(100) NOT NULL,
`status` char(1) NOT NULL DEFAULT '1',
`role` varchar(10) NOT NULL DEFAULT 'siteadmin',
`aud_ins_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
`aud_upd_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
`aud_ins_user` varchar(100) NOT NULL,
`aud_upd_user` varchar(100) NOT NULL,
PRIMARY KEY (`id_user`),
UNIQUE KEY `username_UNIQUE` (`username`),
KEY `username_INDEX` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;



# Dump of table innydb_user_site
# ------------------------------------------------------------

DROP TABLE IF EXISTS `innydb_user_site`;

CREATE TABLE `innydb_user_site` (
`id_user` varchar(40) NOT NULL,
`id_site` varchar(40) NOT NULL,
`status` char(1) NOT NULL DEFAULT '1',
`permission` longtext NOT NULL,
`aud_ins_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
`aud_upd_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
`aud_ins_user` varchar(100) NOT NULL,
`aud_upd_user` varchar(100) NOT NULL,
UNIQUE KEY `user_site_UNIQUE` (`id_site`,`id_user`),
KEY `id_user_INDEX` (`id_user`),
KEY `id_site_INDEX` (`id_site`),
KEY `user_site_INDEX` (`id_site`,`id_user`),
CONSTRAINT `id_site_FK` FOREIGN KEY (`id_site`) REFERENCES `innydb_site` (`public_id`) ON UPDATE CASCADE,
CONSTRAINT `id_user_FK` FOREIGN KEY (`id_user`) REFERENCES `innydb_user` (`username`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;



# Dump of table innydb_version
# ------------------------------------------------------------

DROP TABLE IF EXISTS `innydb_version`;

CREATE TABLE `innydb_version` (
`version` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `innydb_version` WRITE;
/*!40000 ALTER TABLE `innydb_version` DISABLE KEYS */;

INSERT INTO `innydb_version` (`version`)
VALUES
(30);

/*!40000 ALTER TABLE `innydb_version` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
