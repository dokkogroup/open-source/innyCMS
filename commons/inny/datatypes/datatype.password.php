<?php
/**
 *
 *
 */
require_once dirname(__FILE__).'/datatype.text.php';

/**
 *
 *
 */
class InnyType_Password extends InnyType_Text {

    /**
     * @var array parametros esperados en la metadata para el tipo con su valor por defecto e indicando si el mismo es requerido o no
     * @static
     * @access protected
     */
    protected static $typeParams =  array();

    /**
     * InnyType_Textarea constructor.
     */
    public function __construct(){
        parent::__construct();
        $this->type = 'password';
    }

    /**
     * Valida el contenido que se le setea al tipo
     *
     * @access public
     * @return array
     */
    public function validate($value = null){
        $return = array();
        $return['msg_list'] = array();

        if($this->hasUnsafeMode()){
            $return['value'] = $value;
            $return['valid'] = self::VALUE_VALID;
            return $return;
        }

        $required = $this->getParamValue('required');
        $multilang = $this->getParamValue('multilang');

        $values = array();
        if($multilang){
            $vals = json_decode($value,true);
            if(!is_array($vals)){
                $return['valid'] = self::VALUE_INVALID;
                $return['msg_list'][] = static::$errorMessages[self::VALUE_ERROR_EMPTY];
                return $return;
            }
            $languages = $this->getParamValue("languages");
            foreach($languages as $key => $lang){
                $passwordInfo = password_get_info($vals[$key]);
                if($passwordInfo['algo'] === PASSWORD_DEFAULT) $values[$key] = $vals[$key];
                else $values[$key] = password_hash($vals[$key],PASSWORD_DEFAULT);
            }
        }else{
            $passwordInfo = password_get_info($value);
            if($passwordInfo['algo'] === PASSWORD_DEFAULT) $values[] = $value;
            else $values[] = password_hash($value,PASSWORD_DEFAULT);
        }

        $validValues = self::VALUE_VALID;

        foreach($values as $key => $val){
            $val = Denko::trim($val);
            if(empty($val) && $required){
                $return['msg_list'][$key][] = static::$errorMessages[self::VALUE_ERROR_EMPTY];
                $validValues = self::VALUE_INVALID;
            }
        }

        if($validValues) {
            if($multilang){
                $newValue = json_encode($values);
            } else {
                $newValue = $values[0];
            }
        } else {
            $newValue = $value;
        }
        
        $return['value'] = $newValue;
        $return['valid'] = $validValues;

        return $return;
    }
}
################################################################################
?>