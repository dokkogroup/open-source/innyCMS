# Initial setup:

1. Set repository as submodule on your own project under the folder innyCMS.
2. Open /innyCMS/install folder on your browser.

# License

The MIT License (MIT) - [LICENSE.md](LICENSE.md)

Copyright &copy; 2017 DokkoGroup &  Ezequiel Geringer (http://www.dokkogroup.com)
